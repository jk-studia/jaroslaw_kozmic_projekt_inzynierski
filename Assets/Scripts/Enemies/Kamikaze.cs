﻿using System;
using Common;
using Players;
using UnityEngine;

namespace Enemies
{
    public class Kamikaze : AbstractDestructibleObject
    {
        /* FIELDS */
        [SerializeField] private LayerMask collisionsLayer;
        
        private Animator animator;
        private PolygonCollider2D polygonCollider2D;
        private Transform kamikazeTransform;
        private Vector3 scanVector;
        private Vector3 targetPosition;
        private Vector3 startPosition;
        private Vector3 positionOffset;
        private float distance;
        private bool targetFound;
        private string minionName;
        
        /* CONSTRUCTORS AND INITIALIZERS */
        public void Init(string minionName)
        {
            this.minionName = minionName;
        }
        
        /* EVENT FUNCTIONS */
        protected override void Start()
        {
            base.Start();
            animator = GetComponent<Animator>();
            polygonCollider2D = GetComponent<PolygonCollider2D>();
            distance = 20f;
            targetFound = false;
            scanVector = new Vector3(0, -distance, 0);
            kamikazeTransform = transform;
            startPosition = kamikazeTransform.localPosition;
            positionOffset = startPosition + new Vector3(0f, -2.5f);
        }
        
        private void FixedUpdate()
        {
            if (targetFound)
            {
                if (rigidbody2D.velocity.sqrMagnitude <= float.Epsilon)
                {
                    MoveTowardsTarget();
                }
            }
            else
            {
                FindTarget();
            }
        }
        
        protected override void Update()
        {
            base.Update();
            if (!targetFound && !ReferenceEquals(minionName, null))
            {
                float step = speed/2 * Time.deltaTime;
                startPosition = transform.localPosition;
                transform.localPosition = Vector3.MoveTowards(startPosition, positionOffset, step);
            }
        }
        
        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (!ReferenceEquals(transform.parent, null) && !ReferenceEquals(GetComponentInParent<MotherShip>(), null))
            {
                var parent = GetComponentInParent<MotherShip>();
                parent.NotifyAboutMinionDestruction(minionName);
            }
        }
        
        /* METHODS */
        private void FindTarget()
        {
            Vector3 position = kamikazeTransform.position;
            kamikazeTransform.Rotate(0,0,3);
            Vector3 rotatedVector = kamikazeTransform.rotation * scanVector;
            
            polygonCollider2D.enabled = false;
            RaycastHit2D hit = Physics2D.Linecast(position, position+rotatedVector, collisionsLayer);
            Debug.DrawLine(position, position+rotatedVector);
            polygonCollider2D.enabled = true;
            
            if (!ReferenceEquals(hit.transform, null))
            {
                targetFound = true;
                animator.SetBool("targetFound", targetFound);
                targetPosition = hit.transform.position;
            }
        }

        private void MoveTowardsTarget()
        {
            // Debug.DrawLine(targetPosition,kamikazeTransform.position);
            rigidbody2D.AddForce((targetPosition-kamikazeTransform.position).normalized*10, ForceMode2D.Impulse);
        }
    }
}