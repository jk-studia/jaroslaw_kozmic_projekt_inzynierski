﻿using UnityEngine;

namespace Enemies
{
    public class MotherShipAnimationBehaviour : StateMachineBehaviour
    {
        /* FIELDS */
        [SerializeField] private Transform smallExplosion;
        
        /* EVENT FUNCTIONS */
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            Collider2D[] colliders2D = animator.gameObject.GetComponents<Collider2D>();
            Transform explosion = Instantiate(smallExplosion, animator.transform);
            if (stateInfo.IsName("MotherShipFirstDamage"))
            {
                explosion.position += new Vector3(-2.5f, -0.75f);
                colliders2D[colliders2D.Length - 2].enabled = false;
            }
            if (stateInfo.IsName("MotherShipSecondDamage"))
            {
                explosion.position += new Vector3(0.8f, -4f);
                colliders2D[colliders2D.Length - 1].enabled = false;
            }
        }
    }
}
