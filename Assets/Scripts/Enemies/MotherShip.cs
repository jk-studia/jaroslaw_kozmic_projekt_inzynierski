﻿using System;
using System.Collections;
using System.Collections.Generic;
using Common;
using UnityEngine;

namespace Enemies
{
    public class MotherShip : AbstractShootingParticipant
    {
        
        /* FIELDS */
        [SerializeField] private Kamikaze kamikaze;
        [SerializeField] private LayerMask collisions;
        
        private const float MinionScale = 1 / 3f;
        private static readonly int hpPercentage = Animator.StringToHash("HpPercentage");
        
        private Animator animator;
        private Transform player;
        private CompositeCollider2D compositeCollider2D;
        private HashSet<string> spawnedMinions;
        private Dictionary<string, Vector3> spawnLocations;
        private float distance;
        private bool isShooting;
        private float startHp;
       
        /* EVENT FUNCTIONS */
        protected override void Start()
        {
            base.Start();
            explosionScale = new Vector3(3f,3f,1f);
            animator = GetComponent<Animator>();
            compositeCollider2D = GetComponent<CompositeCollider2D>();
            distance = Laser.Distance;
            startHp = hp;
            spawnedMinions = new HashSet<string>();
            spawnLocations = new Dictionary<string, Vector3> {{"RIGHT_MINION", new Vector3(4.2f, 0f, 0f)},
                                                              {"LEFT_MINION", new Vector3(-4.2f, 0f, 0f)}};
        }

        protected override void Update()
        {
            if (hp <= 0)
            {
                compositeCollider2D.enabled = false;
            }
            animator.SetFloat(hpPercentage, CalculateHpPercentage());
            if (CheckLaserTarget())
            {
                if (!isShooting)
                {
                    StartCoroutine(ShootLaser());
                }
            }

            foreach (var side in spawnLocations)
            {
                if (CheckSpawnTarget(side.Value))
                {
                    bool exist = false;
                    foreach (var minionName in spawnedMinions)
                    {
                        if (minionName.Equals(side.Key))
                        {
                            exist = true;
                        }
                    }
                    if (!exist) StartCoroutine(SpawnMinion(side));
                }    
            }
            
        }
        
        /* METHODS */
        private float CalculateHpPercentage()
        {
            return hp * 100 / startHp;
        }

        private bool CheckLaserTarget()
        {
            var position = transform.position;
            for (int i = 0; i < gunsTransforms.Length ; i++)
            {
                var startPosition = position + gunsTransforms[i];
                var endPosition = (startPosition - new Vector3(0f,distance,0f));
                RaycastHit2D hit = Physics2D.Linecast(startPosition, endPosition, collisions);
                Debug.DrawLine(startPosition, endPosition);
                if (!ReferenceEquals(hit.transform, null))
                {
                    return true;
                }
            }
            return false;
        }
        
        protected override IEnumerator ShootLaser()
        {
            isShooting = true;
            Vector3 position = transform.position;
            Laser[] enemyLasers = new Laser[gunsTransforms.Length];
            for (int i = 0; i < gunsTransforms.Length; i++)
            {
                enemyLasers[i] = CreateLaser();
                enemyLasers[i].transform.position = position + gunsTransforms[i];
                enemyLasers[i].transform.localScale = new Vector3(0.5f, 0.5f, 0f);
                enemyLasers[i].gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(1.5f);
            isShooting = false;
            
        }
        
        private bool CheckSpawnTarget(Vector3 spawnLineCastOffset)
        {
            var position = transform.position;
            var startPosition = position + spawnLineCastOffset;
            var endPosition = (startPosition - new Vector3(0f,distance,0f));
            RaycastHit2D hit = Physics2D.Linecast(startPosition, endPosition, collisions);
            Debug.DrawLine(startPosition, endPosition);
            return !ReferenceEquals(hit.transform, null);
        }
        
        private IEnumerator SpawnMinion(KeyValuePair<string, Vector3> side)
        {
            spawnedMinions.Add(side.Key);
            var minion = Instantiate(kamikaze, transform);
            minion.Init(side.Key);
            var minionTransform = minion.transform;
            minionTransform.localScale = new Vector3(MinionScale, MinionScale, 0);
            minionTransform.position += 2*side.Value+new Vector3(0,3f,0);
            yield return null;
        }

        public void NotifyAboutMinionDestruction(string minionName)
        {
            spawnedMinions.Remove(minionName);
        }
    }
}