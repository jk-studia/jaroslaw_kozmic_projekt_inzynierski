﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace Common
{
    public abstract class AbstractShootingParticipant : AbstractDestructibleObject
    {
        /* FIELDS */
        [SerializeField] protected Vector3[] gunsTransforms;
        [FormerlySerializedAs("laserInfo")] [SerializeField] private LaserParameters laserParameters;
        
        /* METHODS */
        protected abstract IEnumerator ShootLaser();
        
        protected Laser CreateLaser()
        {
            Laser localeLaser = Instantiate(laserParameters.laserPrefab, transform);
            localeLaser.Init(tag,  rigidbody2D.velocity.y, laserParameters.color);
            return localeLaser;
        }
        
        /* NESTED */       
        [Serializable]
        private struct LaserParameters
        {
            public Laser laserPrefab;
            public Color color;
        }
    }
}