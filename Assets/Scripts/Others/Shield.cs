﻿using System;
using System.Collections;
using Common;
using UnityEngine;

namespace Others
{
    public class Shield : MonoBehaviour
    {
        /* FIELDS */
        private const float ShieldDuration = 10.0f;
        private GameObject parent;

        /* EVENT FUNCTIONS */
        public void Start()
        {
            parent = GetComponentsInParent<Transform>()[1].gameObject;
            ChangeParentPhysics2DLayer(Layer.IgnoreCollisions.GetHashCode());
            StartCoroutine(StayActive());
        }

        private void OnDestroy()
        {
            ChangeParentPhysics2DLayer(Layer.PlayerCollisions.GetHashCode());
        }
        
        /* METHODS */
        private IEnumerator StayActive()
        {
            yield return new WaitForSeconds(ShieldDuration);
            Destroy(gameObject);
        }

        private void ChangeParentPhysics2DLayer(int physics2DLayer)
        {
            parent.layer = physics2DLayer;
        }
    }
}