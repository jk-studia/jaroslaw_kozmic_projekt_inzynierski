﻿using System;
using Common;
using UnityEngine;
using Random = System.Random;

namespace Others
{
    public class Asteroid : MonoBehaviour
    {
        /* FIELDS */
        [SerializeField] private Sprite[] asteroidSprites;

        private SpriteRenderer spriteRenderer;

        /* CONSTRUCTORS AND INITIALIZERS */
        public void Init()
        {
            Random random = new Random();
            int index = random.Next(asteroidSprites.Length);
            spriteRenderer.sprite = asteroidSprites[index];
        }
        
        /* EVENT FUNCTIONS */
        protected void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.layer.Equals(Layer.GameBorder.GetHashCode()))
            {
                Destroy(gameObject);
            }
        }
    }
}