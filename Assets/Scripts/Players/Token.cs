﻿using Common;
using UnityEngine;
using UnityEngine.Serialization;

namespace Players
{
    public class Token : MonoBehaviour
    {
        /* FIELDS */
        [FormerlySerializedAs("tokenTypes")] [FormerlySerializedAs("weaponTier")] public TokenType tokenType;
        
        /* EVENT FUNCTIONS */
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.gameObject.CompareTag("Player"))
            {
                return;
            }
            other.gameObject.GetComponent<Player>().ActivateTokenEffect(tokenType);
            Destroy(gameObject);
        }
    }
}