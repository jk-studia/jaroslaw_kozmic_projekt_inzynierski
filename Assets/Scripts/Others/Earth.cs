﻿using System;
using Common;
using Players;
using UnityEngine;
using UnityEngine.Playables;

namespace Others
{
    public class Earth : MonoBehaviour
    {
        /** FIELDS */
        private ExposedReference<Player> playerExposedReference;
        private Transform player;
        private PlayableDirector playableDirector;
        private float speed = 3;

        /** EVENT FUNCTIONS */
        private void Start()
        {
            playableDirector = GetComponent<PlayableDirector>();
            // playerExposedReference = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.gameObject.CompareTag("Player"))
            {
                return;
            }
            other.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            player = other.transform;
        }

        private void Update()
        {
            if (ReferenceEquals(player, null))
            {
                return;
            }
            
            float step = speed * Time.deltaTime;
            player.position = Vector3.MoveTowards(player.position, transform.position, step);
            
            if (player.position.Equals(transform.position))
            {
                player.SetParent(gameObject.transform);
                player.gameObject.layer = Layer.IgnoreCollisions.GetHashCode();
                playableDirector.enabled = true;
                enabled = false;
            }
        }
    }
}