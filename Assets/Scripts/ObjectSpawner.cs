﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Enemies;
using Others;
using UnityEngine;
using Random = System.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class ObjectSpawner : MonoBehaviour
{
    /* FIELDS */
    [SerializeField] private Rigidbody2D[] objects;
    [SerializeField] private float spawnDistanceFromPlayer;
    [SerializeField] private Parameters asteroidParameters;
    [SerializeField] private Parameters smallSatelliteParameters;
    [SerializeField] private Parameters bigSatelliteParameters;
    
    private Random randomizer;
    
    /* EVENT FUNCTIONS */
    // Start is called before the first frame update
    private void Start()
    {
        randomizer = new Random();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag("Player"))
        {
            return;
        }
        int index = randomizer.Next(objects.Length);
        Rigidbody2D toInstantiate = objects[index];
        switch (toInstantiate.gameObject.tag)
        {
            case "Asteroid":
            {
                SpawnAsteroid(toInstantiate);
                break;
            }
            case "Enemy":
            {
                SpawnEnemy(toInstantiate); 
                break;
            }
            case "Satellite":
            {
                SpawnSatellite(toInstantiate); 
                break;
            }
        }
        // gameObject.SetActive(false);
    }
    
    /* METHODS */
    private void SpawnAsteroid(Rigidbody2D toInstantiate)
    {
        // Debug.Log("Instantiating Asteroid");
        Rigidbody2D instantiatedObject = Instantiate(toInstantiate);
        instantiatedObject.GetComponent<Asteroid>().Init();
        float oxOffset = GetRandomOXOffset(-25, 25);
        instantiatedObject.transform.position =  new Vector3(oxOffset, spawnDistanceFromPlayer);
        if (oxOffset < -15)
        {
            instantiatedObject.AddForce(new Vector2(1,-1)*asteroidParameters.force, ForceMode2D.Impulse);
            AddTorqueToObject(instantiatedObject, asteroidParameters.torque);
        }
        else if (oxOffset > 15)
        {
            instantiatedObject.AddForce(new Vector2(-1,-1)*asteroidParameters.force, ForceMode2D.Impulse);
            AddTorqueToObject(instantiatedObject, asteroidParameters.torque);
        }
        else
        {
            instantiatedObject.AddForce(Vector2.down*asteroidParameters.force, ForceMode2D.Impulse);
            AddTorqueToObject(instantiatedObject, asteroidParameters.torque);
        }
        
    }

    private void SpawnEnemy(Rigidbody2D toInstantiate)
    {
        // Debug.Log("Instantiating Enemy");
        Rigidbody2D instantiatedObject = Instantiate(toInstantiate);
        instantiatedObject.transform.position =  new Vector3(GetRandomOXOffset(-15, 15), spawnDistanceFromPlayer);
    }
    
    private void SpawnSatellite(Rigidbody2D toInstantiate)
    {
        // Debug.Log("Instantiating Satellite");
        Rigidbody2D instantiatedObject = Instantiate(toInstantiate);
        
        instantiatedObject.transform.position = new Vector3(GetRandomOXOffset(-18, 18), spawnDistanceFromPlayer);
        if (toInstantiate.name.Equals("SmallSatellite"))
        {
            instantiatedObject.AddForce(Vector2.down*smallSatelliteParameters.force, ForceMode2D.Impulse);
            AddTorqueToObject(instantiatedObject, smallSatelliteParameters.torque);
        }
        else
        {
            instantiatedObject.AddForce(Vector2.down*bigSatelliteParameters.force, ForceMode2D.Impulse);
            AddTorqueToObject(instantiatedObject, bigSatelliteParameters.torque);
        }
    }

    private float GetRandomOXOffset(float minValue, float maxValue)
    {
        return (float) randomizer.NextDouble()*(maxValue-minValue)+minValue;
    }
    
    private void AddTorqueToObject(Rigidbody2D instantiatedObject, float torque)
    {
        int sign = randomizer.Next(-1, 2);
        instantiatedObject.AddTorque(sign*torque, ForceMode2D.Impulse);
    }
    
    /* NESTED */
    [Serializable]
    private struct Parameters
    {
        public float force;
        public float torque;
    }
}
