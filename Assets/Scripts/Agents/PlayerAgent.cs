﻿using Common;
using MLAgents;
using UnityEngine;

namespace Agents
{
    public class PlayerAgent : Agent
    {

        /** FIELDS */
        private Rigidbody2D rigidbody2D;
        public float speed = 10;

        
        /** EVENT FUNCTIONS */
        public void Start()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
        }

        public override void CollectObservations()
        {
            AddVectorObs(transform.localPosition);
            AddVectorObs(rigidbody2D.velocity.x);
            AddVectorObs(rigidbody2D.velocity.y);
        }

        public override void AgentAction(float[] vectorAction)
        {
            Vector3 controlSignal = Vector3.zero;
            controlSignal.x = vectorAction[0];
            controlSignal.y = vectorAction[1];
            rigidbody2D.AddForce(controlSignal * speed);
        
            // float distanceToTarget = Vector3.Distance(transform.localPosition,target.localPosition);
            //
            // if (distanceToTarget < 1.42f)
            // {
            //     SetReward(1.0f);
            //     Done();
            // }
            //
            // if (transform.localPosition.y < 0)
            // {
            //     Done();
            // }
        }

        /** METHODS */
        public override float[] Heuristic()
        {
            var movement = new float[2];
            movement[0] = Input.GetAxis("Horizontal");
            movement[1] = Input.GetAxis("Vertical");
            return movement;
        }
    }
    
}
