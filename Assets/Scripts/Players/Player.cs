﻿using System;
using System.Collections;
using Common;
using UnityEngine;

namespace Players
{
    public class Player : AbstractShootingParticipant
    {
        /* FIELDS */
        [SerializeField] private  WeaponTier weaponTier;
        [SerializeField] private GameObject shield;
        
        private static readonly int tilt = Animator.StringToHash("Tilt");
        
        private Animator animator;
        private Vector2 movement;

        /* EVENT FUNCTIONS */
        private void Awake()
        {
            weaponTier = WeaponTier.Tier1;
        }

        protected override void Start()
        {
            base.Start();
            animator = GetComponent<Animator>();
            movement = Vector2.zero;
        }

        private void FixedUpdate()
        {
            if (movement == Vector2.zero) 
                return;
            
            if ((rigidbody2D.velocity.y >= speed && movement.y.Equals(1)) || 
                (rigidbody2D.velocity.y <= -speed && movement.y.Equals(-1)))
            {
                movement.y = 0;
            }
            
            if ((rigidbody2D.velocity.x >= speed && movement.x.Equals(1)) || 
                (rigidbody2D.velocity.x <= -speed && movement.x.Equals(-1)))
            {
                movement.x = 0;
            }
            
            rigidbody2D.AddForce(movement, ForceMode2D.Impulse);
            movement = Vector2.zero;
        }
        
        protected override void Update()
        {
            base.Update();
            MoveWithKeyboard();
            StartCoroutine(ShootLaser());
        }
        
        /* METHODS */
        protected override IEnumerator ShootLaser()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Laser[] lasers = null;
                Vector3 playerPosition = transform.position;
                switch (weaponTier)
                {
                    case WeaponTier.Tier1:
                        lasers = new Laser[1];
                        lasers[0] = CreateLaser();
                        lasers[0].transform.position = playerPosition + gunsTransforms[1];
                        break;
                    case WeaponTier.Tier2:
                        lasers = new Laser[2];
                        lasers[0] = CreateLaser();
                        lasers[1] = CreateLaser();
                        lasers[0].transform.position = playerPosition + gunsTransforms[0];
                        lasers[1].transform.position = playerPosition + gunsTransforms[2];
                        break;
                    case WeaponTier.Tier3:
                        lasers = new Laser[3];
                        lasers[0] = CreateLaser();
                        lasers[1] = CreateLaser();
                        lasers[2] = CreateLaser();
                        lasers[0].transform.position = playerPosition + gunsTransforms[0];
                        lasers[1].transform.position = playerPosition + gunsTransforms[1];
                        lasers[2].transform.position = playerPosition + gunsTransforms[2];
                        break;
                }
                if (lasers != null)
                {
                    foreach (var laser in lasers)
                    {
                        laser.gameObject.SetActive(true);
                    }
                }
            }
            yield return null;
        }
        
        private void MoveWithKeyboard()
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            movement.y = Input.GetAxisRaw("Vertical");
            animator.SetFloat(tilt, movement.x);
        }
        
        public void ActivateTokenEffect(TokenType tokenType)
        {
            if (tokenType.Equals(TokenType.Shield))
            {
                CreateShield();
            }
            else
            {
                SetWeaponTier(tokenType);
            }
        }

        private void CreateShield()
        {
            Instantiate(shield, transform);
        }

        private void SetWeaponTier(TokenType tokenType)
        {
            if (tokenType.Equals(TokenType.WeaponTier2))
            {
                weaponTier = WeaponTier.Tier2;
            }
            if (tokenType.Equals(TokenType.WeaponTier3))
            {
                weaponTier = WeaponTier.Tier3;
            }
        }
        
        /* NESTED */                        
        [Serializable]
        private enum WeaponTier
        {
            Tier1,
            Tier2,
            Tier3
        }
    }
}

