﻿using System.Collections;
using Common;
using UnityEngine;

namespace Enemies
{
    public class Soldier : AbstractShootingParticipant
    {
        /* FIELDS */
        private bool isShooting;
        
        /* EVENT FUNCTIONS */
        protected override void Start()
        {
            base.Start();
            isShooting = false;
        }
        
        // todo działanie: Slider joint
        private void FixedUpdate()
        {
            // if (Vector3.Distance(player.position, transform.position) <= distance + 10)
            // {
            //     
            // }
        }

        protected override void Update()
        {
            base.Update();
            {
                if (!isShooting)
                {
                    StartCoroutine(ShootLaser());
                }
            }
        }

        /* METHODS */
        protected override IEnumerator ShootLaser()
        {
            isShooting = true;
            Laser enemyLaser = CreateLaser();
            enemyLaser.transform.position = transform.position + gunsTransforms[0];
            enemyLaser.gameObject.SetActive(true);
            yield return new WaitForSeconds(2f);
            isShooting = false;
        }
    }
}

