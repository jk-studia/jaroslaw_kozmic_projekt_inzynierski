﻿using UnityEngine;

namespace Common
{
    public class ExplosionBehaviour : StateMachineBehaviour
    {
        
        /* EVENT FUNCTIONS */
        /* OnStateEnter is called when a transition starts and the state machine starts to evaluate this state */
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            Destroy(animator.gameObject, stateInfo.length);
        }
        
    }
}
