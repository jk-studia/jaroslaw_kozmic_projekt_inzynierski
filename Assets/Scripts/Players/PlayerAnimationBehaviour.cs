﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Players
{
    public class PlayerAnimationBehaviour : StateMachineBehaviour
    {
        /* EVENT FUNCTIONS */
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            Collider2D[] colliders2D = animator.gameObject.GetComponents<Collider2D>();

            // Idle collider index
            int activeColliderIndex = 0;
            
            if (stateInfo.IsName("TiltLeft"))
            {
                activeColliderIndex = 1;
            }
            if (stateInfo.IsName("TiltLeftMax"))
            {
                activeColliderIndex = 2;
            }
            if (stateInfo.IsName("TiltRight"))
            {
                activeColliderIndex = 3;
            }
            if (stateInfo.IsName("TiltRightMax"))
            {
                activeColliderIndex = 4;
            }

            for (int i = 0; i < colliders2D.Length; i++)
            {
                if (i == activeColliderIndex)
                {
                    colliders2D[i].enabled = true;
                    continue;
                }
                colliders2D[i].enabled = false;
            }
        }
    }
}

