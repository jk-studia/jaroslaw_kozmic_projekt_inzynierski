﻿using System;
using UnityEngine;

namespace Common
{
    public class Laser : MonoBehaviour
    {
        /* FIELDS */
        [SerializeField] private GameObject laserExplosion;
        
        public const float Distance = 25f;
        public const float LaserSpeed = 25f;
        public const float DamageValue = 1f;
        private Color color;
        private Rigidbody2D rigidbody2D;
        private Vector3 startPosition;
        private string shooterTag;
        private float participantSpeed;
        
        /* CONSTRUCTORS AND INITIALIZERS */
        public void Init(string shooterTag,  float participantSpeed, Color color)
        {
            this.shooterTag = shooterTag;
            this.participantSpeed = participantSpeed;
            this.color = color;
        }

        /* EVENT FUNCTIONS */
        private void Start()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
            GetComponent<SpriteRenderer>().color = color;
            rigidbody2D.velocity = Vector2.zero;
            startPosition = transform.position;
        }

        private void FixedUpdate()
        {
            if (rigidbody2D.velocity.Equals(Vector2.zero))
            {
                Vector2 movement = shooterTag.Equals("Player") ? Vector2.up : Vector2.down;
                rigidbody2D.AddForce(movement*LaserSpeed, ForceMode2D.Impulse);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {

            if (other.tag.Equals(shooterTag) || other.isTrigger
                                             || (other.CompareTag("Shield") && other.GetComponentsInParent<Transform>()[1].CompareTag(shooterTag))
                                             || other.CompareTag("MainCamera"))
            {
                return;
            }

            var localeLaserExplosion = Instantiate(laserExplosion, transform.position, Quaternion.identity);
            localeLaserExplosion.GetComponent<SpriteRenderer>().color = color;
            laserExplosion.transform.localScale = new Vector3(1, 1, 1);
            laserExplosion.SetActive(true);
            
            if (other.GetComponent<AbstractDestructibleObject>() != null)
            {
                DealDamage(other);
            }
            
            Destroy(gameObject);
        }
        
        private void Update()
        {
            if (Vector3.Distance(startPosition, transform.position) >= Distance)
            {
                Destroy(gameObject);    
            }
        }
        
        /* METHODS */
        private void DealDamage(Collider2D other)
        {
            var hitObject = other.GetComponent<AbstractDestructibleObject>();
            if (hitObject != null)
            { 
                hitObject.GetDamage(DamageValue);
            }
        }
    }
}
