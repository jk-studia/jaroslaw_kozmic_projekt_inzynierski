﻿using System;
using System.Collections.Generic;
using Common;
using UnityEngine;
using Random = System.Random;

namespace Others
{
    public class Satellite : AbstractDestructibleObject
    {
        /* FIELDS */
        [SerializeField] private GameObject[] tokens;
        [SerializeField] private Sprite[] sprites;

        private SpriteRenderer spriteRenderer;
        private PolygonCollider2D[] polygonColliders2D;
        private float partHp;
        private int stateIndex;
        private int hpMultiplier;
        
        /* EVENT FUNCTIONS */
        protected void Start()
        {
            base.Start();
            explosionScale = new Vector3(1, 1, 1);
            spriteRenderer = GetComponent<SpriteRenderer>();
            polygonColliders2D = GetComponents<PolygonCollider2D>();
            partHp = hp/sprites.Length;
            hpMultiplier = sprites.Length - 1;
            stateIndex = 0;
        }
        
        protected void LateUpdate()
        {
            if (hpMultiplier <= 0 || hp > hpMultiplier * partHp) 
                return;
            stateIndex++;
            spriteRenderer.sprite = sprites[stateIndex];
            for (int i = 0; i < polygonColliders2D.Length; i++)
            {
                if (i == stateIndex)
                {
                    polygonColliders2D[i].enabled = true;
                    continue;
                }
                polygonColliders2D[i].enabled = false;
            }
            hpMultiplier--;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if(isQuitting) 
                return;
            
            Random random = new Random();
            int index = random.Next(tokens.Length);
            Instantiate(tokens[index], transform.position, Quaternion.identity);
        }

    }
}