﻿namespace Common
{
    public enum Layer
    {
        Triggers = 8,
        Collisions,
        IgnoreCollisions,
        Camera,
        PlayerCollisions,
        GameBorder
    }
}