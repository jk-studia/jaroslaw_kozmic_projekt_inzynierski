﻿using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    public class AbstractDestructibleObject : MonoBehaviour
    {
        
        /* FIELDS */
        [SerializeField] protected float hp = 1;
        [SerializeField] protected float speed = 10;
        [SerializeField] private GameObject explosion;
        
        protected Rigidbody2D rigidbody2D;
        protected Vector3 explosionScale;
        protected bool isQuitting;
        private HashSet<string> collisionImmuneObjects;
        private HashSet<string> collisionSafeObjects;

        /* EVENT FUNCTIONS */
        protected virtual void Start()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
            explosionScale = new Vector3(2, 2, 1);
            collisionImmuneObjects = new HashSet<string> {"MotherShip", "Asteroid"};
            collisionSafeObjects = new HashSet<string> {"MainCamera", "Shield"};
        }
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (collisionImmuneObjects.Contains(gameObject.name) || collisionSafeObjects.Contains(other.gameObject.tag))
            {
                return;
            }
            
            hp = 0;
        }
        
        protected virtual void Update()
        {
            if (hp > 0.0f)
            {
                return;
            }
            
            transform.DetachChildren();
            Destroy(gameObject);
        }
        
        private void OnApplicationQuit()
        {
            isQuitting = true;
        }
        
        protected virtual void OnDestroy()
        {
            if (isQuitting)
            {
                return;
            }
            
            explosion = Instantiate(explosion, transform.position, Quaternion.identity);
            explosion.transform.localScale = explosionScale;
            explosion.SetActive(true);
        }
        
        /* METHODS */
        public void GetDamage(float damage)
        {
            hp -= damage;
        }
    }
}