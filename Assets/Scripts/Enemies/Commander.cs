﻿using System.Collections;
using Common;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace Enemies
{
    public class Commander : AbstractShootingParticipant
    {
        /* FIELDS */
        [SerializeField] private LayerMask collisions;
        
        private Transform player;
        private PolygonCollider2D polygonCollider2D;
        private float distance;
        private bool isShooting;
        
        /* EVENT FUNCTIONS */
        protected override void Start()
        {
            base.Start();
            player = GameObject.FindGameObjectWithTag("Player").transform;
            polygonCollider2D = GetComponent<PolygonCollider2D>();
            distance = Laser.Distance;
            isShooting = false;
        }

        // todo inne działanie, ale ogólnie strzelanie do gracza, może jakiś joint, zastanowić się jeszcze nad efektorami
        private void FixedUpdate()
        {
            // if (Vector3.Distance(player.position, transform.position) <= distance + 10)
            // {
            //     
            // }
        }

        protected override void Update()
        {
            base.Update();
            var position = transform.position;
            polygonCollider2D.enabled = false;
            RaycastHit2D hit = Physics2D.Linecast(new Vector2(position.x, position.y),
                new Vector2(position.x, position.y - distance), collisions);
            Debug.DrawLine(position, new Vector2(position.x, position.y - distance));
            polygonCollider2D.enabled = true;
            if (!ReferenceEquals(hit.transform, null))
            {
                if (!isShooting)
                {
                    StartCoroutine(ShootLaser());
                }
            }
        }

        /* METHODS */
        protected override IEnumerator ShootLaser()
        {
            isShooting = true;
            Vector3 position = transform.position;
            Laser[] enemyLasers = new Laser[2];
            enemyLasers[0] = CreateLaser();
            enemyLasers[1] = CreateLaser();
            enemyLasers[0].transform.position = position + gunsTransforms[0];
            enemyLasers[1].transform.position = position + gunsTransforms[1];
            enemyLasers[0].gameObject.SetActive(true);
            enemyLasers[1].gameObject.SetActive(true);  
            yield return new WaitForSeconds(1.5f);
            isShooting = false;
        }
    }
}