﻿using UnityEngine;

namespace Enemies
{
    public class MotherShipDestructionBehaviour : StateMachineBehaviour
    {
        /* FIELDS */
        [SerializeField] private Transform[] explosionPrefab;

        private Transform[] explosion;
        private static int sign = 1;
    
        /* EVENT FUNCTIONS */
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            explosion = new Transform[explosionPrefab.Length];
            for (int i = 0; i < explosionPrefab.Length; i++)
            {
                explosion[i] = Instantiate(explosionPrefab[i], animator.gameObject.transform);
            }
            if (stateInfo.IsName("MotherShipDestruction_1"))
            {
                explosion[0].transform.position += new Vector3(-2f,3.5f);
                explosion[0].localScale = new Vector3(1.5f,1.5f);
                explosion[1].transform.position += new Vector3(5f,0.5f);
            }        
            if (stateInfo.IsName("MotherShipDestruction_2"))
            {
                explosion[0].transform.position += new Vector3(2.2f,2f);
                explosion[1].transform.position += new Vector3(-4.5f,0.5f);
            }
            if (stateInfo.IsName("MotherShipDestruction_3"))
            {
                explosion[0].transform.position += new Vector3(-2f,-3.8f);
            }
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.transform.position += sign*Time.deltaTime*new Vector3(10f,0f,0f);
            sign *= -1;
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        public override  void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (stateInfo.IsName("MotherShipDestruction_3"))
            {
                Destroy(animator.gameObject);
            }
        }
    
    }
}
